# Project Name: Blockclock

## Implementation:
	* HTML
	* CSS
	* Javascript
	
## Premise
	* Design a column based representation of time

## Requirements for execution:
	* Any browser- preferably Chrome, extensive testing on other browsers not complete
	
## Completion:
	* Column representation :100%
	* Update behaviour :100%
	* Full clock representation: 66%
		- Hour columns still missing 
		
## Optional completion:
	* Select on color and off color