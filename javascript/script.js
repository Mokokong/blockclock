var on_color = "red";
var off_color = "white";

function changeOnColor( c_args)
{
    switch (c_args) {
      case 1:
        on_color = "blue";
        break;
      case 2:
        on_color = "green";
        break;
      case 3:
        on_color = "purple";
        break;
      default:
        on_color = "red";

    }


}


function changeOffColor( c_args)
{
    switch (c_args) {
      case 1:
        on_color =" #545659";
        break;
      case 2:
        on_color = "#aae047";
        break;
      case 3:
        on_color = "#e09047";
        break;
      default:
        on_color = "white";

    }


}

function RunthisClock()
{
    // GET current time and date
    var date = new Date();
  //  console.log(date);

    let hr = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();

    // mathematical approach
    let unitsec = sec %10;
    let tenthsec = (sec%100- unitsec)/(10);
    let unitmin = min%10;
    let tenthmin = ((min - unitmin)%100)/10;
    let unithr = hr%10;
    let tenthhr =(hr -unithr)/10 ;



    //console.log("sec: "+ sec);
    // console.log("unitsec" + unitsec);
    // console.log("tenthsec :" + tenthsec );
    //console.log("min: "+ min);
    // console.log("unitmin: "+ unitmin);
    // console.log("tenthmin: "+ tenthmin);

    //console.log("hr: "+ hr);
    // console.log("unithr: "+ unithr);
    // console.log("tenthhr: "+ tenthhr);



    // string approach
    // let stringsec = sec.toString();
    // let tenthsec = stringsec.charAt(0);
    // let unitsec =  stringsec.charAt(1);
    // console.log("sec: "+ stringsec);
    // console.log("Index[0] :" + tenthsec);


    // unitsec display
    // create array of cell nodelists
    var secblocks =
    [
      document.querySelectorAll(".cellsec1"),
      document.querySelectorAll(".cellsec2"),
      document.querySelectorAll(".cellsec3"),
      document.querySelectorAll(".cellsec4"),
      document.querySelectorAll(".cellsec5"),
      document.querySelectorAll(".cellsec6"),
      document.querySelectorAll(".cellsec7"),
      document.querySelectorAll(".cellsec8"),
      document.querySelectorAll(".cellsec9")



    ];
    // test
    //console.log(secblocks);


    //  tenthsec display
    var tensecblocks =[
      document.querySelectorAll(".celltensec1"),
      document.querySelectorAll(".celltensec2"),
      document.querySelectorAll(".celltensec3"),
      document.querySelectorAll(".celltensec4"),
      document.querySelectorAll(".celltensec5"),
      document.querySelectorAll(".celltensec6"),
      document.querySelectorAll(".celltensec7"),
      document.querySelectorAll(".celltensec8"),
      document.querySelectorAll(".celltensec9")




    ]


    var minblocks =
    [
      document.querySelectorAll(".cellmin1"),
      document.querySelectorAll(".cellmin2"),
      document.querySelectorAll(".cellmin3"),
      document.querySelectorAll(".cellmin4"),
      document.querySelectorAll(".cellmin5"),
      document.querySelectorAll(".cellmin6"),
      document.querySelectorAll(".cellmin7"),
      document.querySelectorAll(".cellmin8"),
      document.querySelectorAll(".cellmin9")



    ];

    var tenminblocks =[
      document.querySelectorAll(".celltenmin1"),
      document.querySelectorAll(".celltenmin2"),
      document.querySelectorAll(".celltenmin3"),
      document.querySelectorAll(".celltenmin4"),
      document.querySelectorAll(".celltenmin5"),
      document.querySelectorAll(".celltenmin6"),
      document.querySelectorAll(".celltenmin7"),
      document.querySelectorAll(".celltenmin8"),
      document.querySelectorAll(".celltenmin9")




    ]
    // seconds
    if ((unitsec>0) && (unitsec <10))
     {
       //console.log("unitsec :"+ unitsec);

      for (var i = 0; i < secblocks.length; i++)
       {
          // console.log( secblocks[i].length);



          // switch on corresponding number
          if (i <unitsec)
          {
            for (var j = 0; j < secblocks[i].length; j++)
            {
                // console.log(secblocks[i][j]);
                // console.log( "on :i :"+ i + " j : " + j);
              //  (secblocks[i][j]).style.backgroundColor = "red";

                (secblocks[i][j]).style.backgroundColor = on_color;
            }


          }
          else
          // switch off the rest of the cells
          {
            for (var j = unitsec; j < secblocks[i].length; j++) {
                // console.log(secblocks[i][j]);
                // console.log(" off :i :"+ i + " j : " + j);
                (secblocks[i][j]).style.backgroundColor = "white";


          }


          }




       }



    }

    else {

      for (var i = 0; i < secblocks.length; i++)
       {
          // console.log( secblocks[i].length);

          // no need to check case number because at 0 all are off

          for (var j = 0; j < secblocks[i].length; j++) {
              // console.log(secblocks[i][j]);
              // console.log("i :"+ i + " j : " + j);
              (secblocks[i][j]).style.backgroundColor = "white";
          }




       }

    }
    // end of unitsec display



    if ((tenthsec > 0) && (tenthsec <10))
    {

      for (var i = 0; i < tensecblocks.length; i++)
       {
          // console.log( secblocks[i].length);

          // switch on corresponding number
          if (i <tenthsec)
          {
            for (var j = 0; j < tensecblocks[i].length; j++)
            {
                // console.log(secblocks[i][j]);
                // console.log( "on :i :"+ i + " j : " + j);
                (tensecblocks[i][j]).style.backgroundColor = "red";
            }


          }
          else
          // switch off the rest of the cells
          {
            for (var j = tenthsec; j < tensecblocks[i].length; j++)
            {

                // console.log(" off :i :"+ i + " j : " + j);
                (tensecblocks[i][j]).style.backgroundColor = "white";


          }


          }

       }

    }
    else
    {
      for (var i = 0; i < tensecblocks.length; i++)
      {
          // console.log( secblocks[i].length);

          // no need to check case number because at 0 all are off

          for (var j = 0; j < tensecblocks[i].length; j++)
           {
             // console.log(secblocks[i][j]);
             // console.log("i :"+ i + " j : " + j);
            (tensecblocks[i][j]).style.backgroundColor = "white";
          }

      }

    } // end of tensec display

    //mins

    if ((unitmin > 0) && (unitmin <10))
    {

      for (var i = 0; i < minblocks.length; i++)
       {


          // switch on corresponding number
          if (i <unitmin)
          {
            for (var j = 0; j < minblocks[i].length; j++)
            {
                // console.log(secblocks[i][j]);
                // console.log( "on :i :"+ i + " j : " + j);
                (minblocks[i][j]).style.backgroundColor = "red";
            }


          }
          else
          // switch off the rest of the cells
          {
            for (var j = unitmin; j < minblocks[i].length; j++)
             {
                // console.log(secblocks[i][j]);
                // console.log(" off :i :"+ i + " j : " + j);
                (minblocks[i][j]).style.backgroundColor = "white";


          }


          }
        }
    }
    else
     {


        for (var i = 0; i < minblocks.length; i++)
          {

             // no need to check case number because at 0 all are off

            for (var j = 0; j < minblocks[i].length; j++)
              {
                     (minblocks[i][j]).style.backgroundColor = "white";
              }




               }
    }
      // end of unitmin display

    if ((tenthmin >0)&&(tenthmin<10))
    {

      for (var i = 0; i < tenminblocks.length; i++)
       {


          // switch on corresponding number
          if (i <tenthmin)
          {
            for (var j = 0; j < tenminblocks[i].length; j++)
            {
                // console.log(secblocks[i][j]);
                // console.log( "on :i :"+ i + " j : " + j);
                (tenminblocks[i][j]).style.backgroundColor = "red";
            }


          }
          else
          // switch off the rest of the cells
          {
            for (var j = tenthmin; j < tenminblocks[i].length; j++)
            {
                // console.log(secblocks[i][j]);
                // console.log(" off :i :"+ i + " j : " + j);
                (tenminblocks[i][j]).style.backgroundColor = "white";


            }


          }




       }


    }

    else
    {

      for (var i = 0; i < tenminblocks.length; i++)
        {


          // no need to check case number because at 0 all are off

          for (var j = 0; j < tenminblocks[i].length; j++)
           {

              (tenminblocks[i][j]).style.backgroundColor = "white";
          }




        }

    }
      // end of tenthmin display

    // switch (unithr) {
    //
    //   case 0:
    //     hblock1.classList.remove("blockon");
    //     hblock2.classList.remove("blockon");
    //     hblock3.classList.remove("blockon");
    //     hblock4.classList.remove("blockon");
    //     hblock5.classList.remove("blockon");
    //     hblock6.classList.remove("blockon");
    //     hblock7.classList.remove("blockon");
    //     hblock8.classList.remove("blockon");
    //     hblock9.classList.remove("blockon");
    //
    //     break;
    //
    //   case 1:
    //
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.remove("blockon");
    //     hblock3.classList.remove("blockon");
    //     hblock4.classList.remove("blockon");
    //     hblock5.classList.remove("blockon");
    //     hblock6.classList.remove("blockon");
    //     hblock7.classList.remove("blockon");
    //     hblock8.classList.remove("blockon");
    //     hblock9.classList.remove("blockon");
    //
    //
    //     break;
    //
    //   case 2:
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.add("blockon");
    //     hblock3.classList.remove("blockon");
    //     hblock4.classList.remove("blockon");
    //     hblock5.classList.remove("blockon");
    //     bhlock6.classList.remove("blockon");
    //     bhlock7.classList.remove("blockon");
    //     hblock8.classList.remove("blockon");
    //     hblock9.classList.remove("blockon");
    //     break;
    //
    //
    //   case 3:
    //
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.add("blockon");
    //     hblock3.classList.add("blockon");
    //     hblock4.classList.remove("blockon");
    //     hblock5.classList.remove("blockon");
    //     hblock6.classList.remove("blockon");
    //     hblock7.classList.remove("blockon");
    //     hblock8.classList.remove("blockon");
    //     hblock9.classList.remove("blockon");
    //     break;
    //
    //   case 4:
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.add("blockon");
    //     hblock3.classList.add("blockon");
    //     hblock4.classList.add("blockon");
    //     hblock5.classList.remove("blockon");
    //     hblock6.classList.remove("blockon");
    //     hblock7.classList.remove("blockon");
    //     hblock8.classList.remove("blockon");
    //     hblock9.classList.remove("blockon");
    //     break;
    //   case 5:
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.add("blockon");
    //     hblock3.classList.add("blockon");
    //     hblock4.classList.add("blockon");
    //     hblock5.classList.add("blockon");
    //     hblock6.classList.remove("blockon");
    //     hblock7.classList.remove("blockon");
    //     hhblock8.classList.remove("blockon");
    //     hblock9.classList.remove("blockon");
    //     break;
    //
    //   case 6:
    //       hblock1.classList.add("blockon");
    //       hblock2.classList.add("blockon");
    //       hblock3.classList.add("blockon");
    //       hblock4.classList.add("blockon");
    //       hblock5.classList.add("blockon");
    //       hblock6.classList.add("blockon");
    //       hblock7.classList.remove("blockon");
    //       hblock8.classList.remove("blockon");
    //       hblock9.classList.remove("blockon");
    //
    //       break;
    //   case 7:
    //
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.add("blockon");
    //     hblock3.classList.add("blockon");
    //     hblock4.classList.add("blockon");
    //     hblock5.classList.add("blockon");
    //     hblock6.classList.add("blockon");
    //     hblock7.classList.add("blockon");
    //     hblock8.classList.remove("blockon");
    //     hblock9.classList.remove("blockon");
    //
    //
    //     break;
    //
    //   case 8:
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.add("blockon");
    //     hblock3.classList.add("blockon");
    //     hblock4.classList.add("blockon");
    //     hblock5.classList.add("blockon");
    //     hblock6.classList.add("blockon");
    //     hblock7.classList.add("blockon");
    //     hblock8.classList.add("blockon");
    //     hblock9.classList.remove("blockon");
    //
    //     break;
    //   case 9:
    //
    //     hblock1.classList.add("blockon");
    //     hblock2.classList.add("blockon");
    //     hblock3.classList.add("blockon");
    //     hblock4.classList.add("blockon");
    //     hblock5.classList.add("blockon");
    //     hblock6.classList.add("blockon");
    //     hblock7.classList.add("blockon");
    //     hblock8.classList.add("blockon");
    //     hblock9.classList.add("blockon");
    //
    //     break;
    //
    //   default:
    //
    //
    //     break;
    //
    // }//end of  unithr switch


}// end of RunthisClock()


var interval = setInterval(
  function(){
  RunthisClock();
  }
  ,1000);
